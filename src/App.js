import React from "react";
import {
    AppBar,
    Container,
    Toolbar,
    IconButton,
    Typography, Box,
    Button,
    Paper,
    Grid,
    Card,
    CardMedia,
    CardContent,
    CardActions,
    BottomNavigation,
    BottomNavigationAction,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    TextField,
    DialogActions
} from "@mui/material";
import LayersIcon from '@mui/icons-material/Layers';
import PlayCircleFilledIcon from '@mui/icons-material/PlayCircleFilled';
import MenuIcon from '@mui/icons-material/Menu';
import RestoreIcon from '@mui/icons-material/Restore';
import FavoriteIcon from '@mui/icons-material/Favorite';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import FolderIcon from '@mui/icons-material/Folder';
import {makeStyles} from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    title: {
        flexGrow: 1
    },
    mainFeaturePost: {
        position: "relative",
        marginBottom: theme.spacing(4),

        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center"
    },
    mainFeaturesPostContent: {
        position: "relative",
        padding: theme.spacing(3),
        color: "#fff"
    },
    cardGrid: {
      marginTop: theme.spacing(4)
    },
    cardMedia: {
        paddingTop: "56.25%"
    },
    cardContent: {
        flexGrow: 1
    }
}));

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9]


function App() {
    const classes = useStyles()

    const [value, setValue] = React.useState('recents');

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const [open, setOpen] = React.useState(false)

    const handleClickOpen = () => {
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
    }

  return (
      <>
          <AppBar position="static">
              <Container fixed>
                  <Toolbar>
                      <IconButton
                          size="large"
                          edge="start"
                          color="inherit"
                          className = {classes.menuButton}
                          aria-label="menu"
                      >
                          <MenuIcon />
                      </IconButton>
                      <Typography variant="h6" className = {classes.title}>Web Developer</Typography>
                      <Box mr={3}>
                          <Button color="inherit" variant="outlined" onClick={handleClickOpen}>Log In</Button>

                          <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                              <DialogTitle id="form-dialog-title">Log in</DialogTitle>
                              <DialogContent>
                                  <DialogContentText>Log in, please!</DialogContentText>
                                  <TextField
                                    autoFocus
                                    margin="dense"
                                    id="name"
                                    label="Email address"
                                    type="email"
                                    fullWidth
                                  />
                                  <TextField
                                      autoFocus
                                      margin="dense"
                                      id="pass"
                                      label="Password"
                                      type="password"
                                      fullWidth
                                  />
                              </DialogContent>
                              <DialogActions>
                                  <Button onClick={handleClose} color="primary">Cencel</Button>
                                  <Button onClick={handleClose} color="primary">Log In</Button>
                              </DialogActions>
                          </Dialog>
                      </Box>
                      <Button color="secondary" variant="contained">Sign Up</Button>
                  </Toolbar>
              </Container>
          </AppBar>

          <main>
              <Paper className={classes.mainFeaturePost}
                     style={{ backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(https://source.unsplash.com/random)`}}>
                  <Container fixed>
                    <Grid container>
                        <Grid item md={6}>
                            <div className={classes.mainFeaturesPostContent}>
                                <Typography
                                    component="h1"
                                    variant="h3"
                                    color="inherit"
                                    gutterBottom
                                >
                                    Web Developer
                                </Typography>
                                <Typography
                                    variant="h5"
                                    color="inherit"
                                    paragraph
                                >
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Asperiores aut commodi dolor ipsa laudantium neque tempora, vitae?
                                    Accusamus accusantium adipisci atque aut eum fuga id impedit possimus quam vero,
                                    voluptas.
                                </Typography>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                >
                                    Learn more...
                                </Button>
                            </div>
                        </Grid>
                    </Grid>
                  </Container>
              </Paper>

              <div className={classes.mainContent}>
                  <Container maxWidth="md">
                      <Typography variant="h2" align="center" color="textPrimary" gutterBottom>Web Developer</Typography>
                      <Typography variant="h6" align="center" color="textSecondary" paragraph>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                          Asperiores aut commodi dolor ipsa laudantium neque tempora, vitae?
                          Accusamus accusantium adipisci atque Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                          Asperiores aut commodi dolor
                      </Typography>
                      <div className={classes.mainButtons}>
                          <Grid container spacing={2} justifyContent="center">
                                <Grid item>
                                    <Button variant="contained" color="primary">Start Now</Button>
                                </Grid>
                              <Grid item>
                                  <Button variant="outlined" color="primary">Learn more</Button>
                              </Grid>
                          </Grid>
                      </div>
                  </Container>
              </div>
              <Container className={classes.cardGrid} maxWidth="md">
                  <Grid container spacing={4}>
                      {cards.map((card) => (
                          <Grid item key={card} xs={12} sm={6} md={4}>
                                <Card className={classes.card}>
                                    <CardMedia
                                        className={classes.cardMedia}
                                        image="https://source.unsplash.com/random"
                                        title="image title"
                                    />
                                    <CardContent className={classes.cardContent}>
                                        <Typography variant="h5" gutterBottom>
                                            Blog post
                                        </Typography>
                                        <Typography>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            Asperiores aut commodi dolor
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button size="small" color="primary">View</Button>
                                        <Button size="small" color="primary">Edit</Button>
                                        <LayersIcon />
                                        <PlayCircleFilledIcon />
                                    </CardActions>
                                </Card>
                          </Grid>
                      ))}
                  </Grid>
              </Container>
          </main>
          <footer>
              <Typography variant="h5" align="center" gutterBottom>Footer</Typography>
              <BottomNavigation
                  value={value}
                  onChange={handleChange}
                  className={classes.root}
              >
                <BottomNavigationAction
                    label="Recents"
                    value="recents"
                    icon={<RestoreIcon />}
                />
                  <BottomNavigationAction
                      label="Favorites"
                      value="favorites"
                      icon={<FavoriteIcon />}
                  />
                  <BottomNavigationAction
                      label="Nearby"
                      value="nearby"
                      icon={<LocationOnIcon />}
                  />
                  <BottomNavigationAction label="Folder" value="folder" icon={<FolderIcon />} />
              </BottomNavigation>
              <Typography align="center" color="textSecondary" component="p" variant="subtitle1">
                  Web Developer Blog
              </Typography>
          </footer>
      </>
  );
}

export default App;
